# Rethinking work #

Notebooks and other work exploring the book and course:
    [_Statistical Rethinking_, by Richard McElreath](http://xcelab.net/rm/statistical-rethinking/)

Initially this is snippets of R code, from `code.txt` provided by McElreath,
copied to Jupyter notebooks running an R kernel.

### Changes and additions ###
* Added start values for map() maximum _a posteriori_ solver.
* Additional regularizing priors for some models to help them converge more reliably.
* More output statements, like pairs() and plot() of map2stan() models.

Dean Elzinga "d c elzinga at-sign gmail (no spaces)"
